!function($) {
    "use strict";

    let NotLoadedYet = true;
    let SampleXmls = ["./sample.xml", ];
    let DepthChartId = '#depth-chart';
    let VelChartId = '#vel-chart';
    let FreefallStartsAt = 20;
    let Divelogs = [];
    // {date:'2019-05-02T09:44:18.05', profile: [ [t0, d0, v0, sv0], [t1, d1, v1, sv1],...]}

    function init_model() {
        let $dfd = $.Deferred();

        const ffat = My.getLocalStorage('FreefallStartsAt');
        if (ffat) FreefallStartsAt = ffat;

        // load sample data
        $.get(SampleXmls[0]).then(function(xml){
            Divelogs.push(xml2jsobj(xml));
            init_view();
            $dfd.resolve();
        }, function(e) {
            console.log('error:', e);
        });
        return $dfd.promise();
    }

    function init_view() {
        init_radio_view();
        init_ff_view();
        init_table_view();
        init_plot_view();
    }

    function init_ff_view() {
        let h = '';
        if (NotLoadedYet) {
            h += '<b>Freefall starts at: &emsp; &emsp; &emsp; &emsp;</b>';
        }
        if (NotLoadedYet || Divelogs.length > 1) {
            let j = Divelogs.length - 1;
            h += '<span>';
            h += `<input type="number" size="2" class="ff-text" id="ff-text${j}" value="${FreefallStartsAt}" style="width:40px;"></input> <b>m</b>`;
            h += '</span> &emsp; &emsp;';
        }
        $('#ff-field').append(h);
        $('.ff-text').change(function() {
            const ffat = $(this).val();
            FreefallStartsAt = ffat;
            My.setLocalStorage('FreefallStartsAt', ffat);
            init_table_view();
        });
    }

    function init_radio_view() {
        let $r = $('#smoothed');
        if (NotLoadedYet) {
            $r.find('.btn[value=smoothed]').addClass('active');
        }
        if (Divelogs.length == 1) {
            $r.hide();
        }
        else {
            $r.show();
        }
        $r.find('.btn').click(function() {
            $r.find('.btn').removeClass('active');
            $(this).addClass('active');
            init_plot_view();
        });
    }

    function init_table_view() {
        let date = [], divet = [], maxd = [], maxdat = [], ffat0 = [];
        let av0 = [], av1 = [], av2 = [];
        for (let j in Divelogs) {
            let p = Divelogs[j].profile;
            let ffat = $('#ff-text' + j).val();
            let md = 0.0, mdat = 0;
            let d0 = 0, t0 = 0;
            for (let i in p) {
                let t = p[i][0];
                let d = p[i][1];
                if (d > md) {
                    md = d;
                    mdat = t;
                }
                if (d0 < ffat && ffat < d) {
                    d0 = d;
                    t0 = t;
                }
            }
            let dt = p[p.length - 1][0]; // the 1st elem of the last elem of profile.
            date[j] = Divelogs[j].date;
            divet[j] = dt;
            maxd[j] = md;
            maxdat[j] = mdat;
            ffat0[j] = d0;
            av1[j] = ((md - d0) / (mdat - t0)).toFixed(2);
            if (t0 > 0) {
                av0[j] = (d0 / t0).toFixed(2);
            }
            else {
                av0[j] = av1[j];
                av1[j] = '-';
            }
            av2[j] = (md / (dt - mdat)).toFixed(2);
        }
        let h = '';
        let rows = [
            ['Start time:', date, ''],
            ['Dive time:', divet, 's'],
            ['Max depth:', maxd, 'm'],
            ['Max depth at :', maxdat, 's'],
            ['Average velocity', '', ''],
            ['&emsp;initial kicking phase:', av0, 'm/s'],
            ['&emsp;freefall to bottom:', av1, 'm/s'],
            ['&emsp;ascent (bottom to surface):', av2, 'm/s'],
        ];
        for (let i in rows) {
            let row = rows[i];
            let bg = '';
            bg = i % 2 == 0 ? '' : 'background-color:#f2f2f2;';
            h += '<tr style="' + bg + '">';
            h += '<th>' + row[0] +'</th>';
            for (let j in Divelogs) {
                h += '<td align="right">' + (row[1] == '' ? '' : row[1][j]) +'</td>';
                h += '<th>' + row[2] +'</th>';
            }
            h += '</tr>';
        }
        $('#info-table').find('tr').remove();
        $('#info-table').append(h);
    }

    function init_plot_view() {
        let legend = [];
        for (let j in Divelogs) {
            legend.push(Divelogs[j].date);
        }

        let dparam = {
            width: 768,
            height: 384,
            margin: { top: 20, right: 50, bottom: 50, left: 70, },
            loader: loader,
            xlabel: 'dive time (s)',
            ylabel: 'depth (m)',
            unit: 'm',
            reverty: true,
            cols: [1,],
            nplots: Divelogs.length,
            legend: legend,
        };
        new Chart(DepthChartId, dparam);

        // set ydomain so that the smoothed curve fits inside the chart.
        let ymin = 100, ymax = 0;
        for (let j in Divelogs) {
            let p = Divelogs[j].profile;
            for (let i in p) {
                let s = p[i][3];
                if (s > ymax) {
                    ymax = s;
                }
                if (s < ymin) {
                    ymin = s;
                }
            }
        }
        let vparam = {
            width: 768,
            height: 384,
            margin: { top: 20, right: 50, bottom: 50, left: 70, },
            loader: loader,
            xlabel: 'dive time (s)',
            ylabel: '|velocity| (m/s)',
            ydomain: [ymin - 0.08, ymax + 0.08],
            unit: 'm/s',
            cols: [3, 2,],
            nplots: Divelogs.length,
            legend: ['smoothed', 'raw', ],
        };
        if (Divelogs.length > 1) {
            let c =$('#smoothed').find('.active').attr('value') == 'smoothed' ? 3 : 2;
            vparam.cols = [c, ];
            vparam.legend = legend;
        }
        new Chart(VelChartId, vparam);
    }

    function loader() {
        let $dfd = new $.Deferred();
        $dfd.resolve(Divelogs);
        return $dfd.promise();
    }

    $('#load-xml-hidden').change(function(event){
        let file = event.target.files[0];
        let reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function(f){
            let text = f.target.result;
            let dp = new DOMParser();
            let xml = dp.parseFromString(text, "application/xhtml+xml");
            if (xml.documentElement.nodeName == "parseerror") alert("failed to parse XML.");
            if (NotLoadedYet) {
                NotLoadedYet = false;                
                $('.sampletext').hide();
                Divelogs = [];
            }
            Divelogs.push(xml2jsobj(xml));
            init_view();
        };
    });

    function xml2jsobj(xml) {
        let format = 'DM5';
        let divelog = {
            date: null,
            profile: [],
        };
        let sample = xml.getElementsByTagName("Dive")[0];
        if (sample) { // assume DM5 format by default
            sample = sample.getElementsByTagName("DiveSamples")[0]
                .getElementsByTagName("Dive.Sample");
            divelog.date = xml.getElementsByTagName("StartTime")[0].childNodes[0].nodeValue;
        }
        else { // next assume MacDive format.
            sample = xml.getElementsByTagName("dives")[0];
            if (sample) {
                format = 'MacDive';
                sample = sample.getElementsByTagName("dive")[0]
                    .getElementsByTagName("sample");
                divelog.date = xml.getElementsByTagName("date")[0].childNodes[0].nodeValue;
            }
            else {
                sample = xml.getElementsByTagName("uddf")[0];
                if (sample) {
                    format = 'MacDiveiOS';
                    sample = sample.getElementsByTagName("profiledata")[0]
                        .getElementsByTagName("repetitiongroup")[0]
                        .getElementsByTagName("dive")[0];
                    divelog.date = sample.getElementsByTagName("datetime")[0].childNodes[0].nodeValue;
                    sample = sample.getElementsByTagName("samples")[0]
                        .getElementsByTagName("waypoint");
                }
                else {
                    alert("Cannot parse the XML file. Only files generated by Suunto DM5 or MacDive can be read.");
                    return null;
                }
            }
        }

        // time and depth
        let time = [], depth = [], vel = [];
        depth[0] = time[0] = 0;
        for (let i = 0; i < sample.length; i++) {
            switch(format) {
            case 'DM5':
                time[i] = +sample[i].getElementsByTagName("Time")[0].childNodes[0].nodeValue;
                depth[i] = +sample[i].getElementsByTagName("Depth")[0].childNodes[0].nodeValue;
                break;
            case 'MacDive':
                time[i] = +sample[i].getElementsByTagName("time")[0].childNodes[0].nodeValue;
                depth[i] = +sample[i].getElementsByTagName("depth")[0].childNodes[0].nodeValue;
                break;
            case 'MacDiveiOS':
                let t = sample[i].getElementsByTagName("time");
                if (t.length == 0) {
                    time[i] = +sample[i].getElementsByTagName("divetime")[0].childNodes[0].nodeValue;
                }
                else {
                    time[i] = t[0].childNodes[0].nodeValue;
                }
                depth[i] = +sample[i].getElementsByTagName("depth")[0].childNodes[0].nodeValue;
                break;
            }
        }

        // velocity
        let vmax = 2.0; // to reject pseudo values caused by quick movement of the device attached to your wrist.
        let vmin = 0.2;
        vel[0] = 0.0;
        for (let i = 1, v = 0.0; i < sample.length; i++) {
            vel[i] = v;
            v = Math.abs(depth[i] - depth[i-1]) / (time[i] - time[i-1]);
            if (v === Infinity || v === -Infinity) v = vel[i];
            if (v > vmax) v = vmax;
            if (v < vmin) v = vmin;
        }

        // smoothed velocity
        let avel = [];
        for (let i = 0; i < sample.length; i++) {
            let w = 10;
            let n = 0;
            avel[i] = 0.0;
            for (let j = -w; j <= +w; j++) {
                if (i+j < 0 || sample.length <= i+j) continue;
                avel[i] += vel[i+j];
                n += 1;
            }
            avel[i] /= n;
        }

        for (let i = 0; i < sample.length; i++) {
            divelog.profile[i] = [time[i], depth[i], vel[i], avel[i], ];
        }
        return divelog;
    }

    $('#load-xml').on('click', function() {
        $('#load-xml-hidden').click();
    });


    $(function init() {
        let dev;
        window.My = new MyUtil();
        init_model();
    });

}(jQuery);
