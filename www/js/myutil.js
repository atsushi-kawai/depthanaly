!function($) {
    "use strict";

    // private, class vars
    var Deploy = true;
    var NSandClock = 0;

    window.onerror = function(msg, url, line) {
        alert(msg + "\n" + url + ":" + line);
    };

    var MyUtil = function() {
        try {
            if ("undefined"== typeof document) return false;
            this.mega = 1024 * 1024;
            this.giga = 1024 * 1024 * 1024;
        }
        catch (e) {
            console.log(e);
        }
    };


    MyUtil.prototype.show_sandclock = function() {
        NSandClock += 1;
        $('#indicator').show();
    };

    MyUtil.prototype.hide_sandclock = function() {
        NSandClock -= 1;
        if (NSandClock <= 0) {
            NSandClock = 0;
            $('#indicator').hide();
        }
    };


    MyUtil.prototype.date_to_string = function(date, format) {
        var monthname = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
        ];
        var dayofweekname = [
            'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
        ];

        if (!format) format = '%Y/%m/%d %H:%M:%S';

        format = format.replace(/%Y/g, date.getFullYear());
        format = format.replace(/%m/g, ('0' + (date.getMonth() + 1)).slice(-2));
        format = format.replace(/%d/g, ('0' + date.getDate()).slice(-2));
        format = format.replace(/%H/g, ('0' + date.getHours()).slice(-2));
        format = format.replace(/%M/g, ('0' + date.getMinutes()).slice(-2));
        format = format.replace(/%S/g, ('0' + date.getSeconds()).slice(-2));
        format = format.replace(/%b/g, monthname[date.getMonth()]);
        format = format.replace(/%a/g, dayofweekname[date.getDay()]);
        return format;
    };

    // truncate src to the nearest dsrc aligned value smaller than src.
    MyUtil.prototype.align_floor = function(src, dsrc) {
        var dst = (Math.floor(src / dsrc)) * dsrc;
        //    console.log("src:" + src + " dst:" + dst + " dsrc:" + dsrc);
        return dst;
    }

    // round up src to the nearest dsrc aligned value larger than src.
    MyUtil.prototype.align_ceil = function(src, dsrc) {
        var dst = (Math.ceil(src / dsrc)) * dsrc;
        //    console.log("src:" + src + " dst:" + dst);
        return dst;
    }

    MyUtil.prototype.storageAvailability =  {
        sessionStorage: null,
        localStorage: null,
    },

    MyUtil.prototype.isStorageAvailable = function(type) {
        let sa = this.storageAvailability[type];
        if (sa !== null) return sa; // already checked                                                                       

        let storage;
        try {
            storage = window[type];
            let x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            this.storageAvailability[type] = true;
            return true;
        }
        catch(e) {
            let ret =  e instanceof DOMException && (
                // everything except Firefox                                                                                 
                e.code === 22 ||
                    // Firefox                                                                                               
                e.code === 1014 ||
                    // test name field too, because code might not be present                                                
                // everything except Firefox                                                                                 
                e.name === 'QuotaExceededError' ||
                    // Firefox                                                                                               
                e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
                // acknowledge QuotaExceededError only if there's something already stored                                   
                (storage && storage.length !== 0);

            this.storageAvailability[type] = ret;
            return ret;
        }
    },

    MyUtil.prototype.setLocalStorage = function(key, obj) {
        if (this.isStorageAvailable('localStorage') !== true) return;
	this.setStorage('localStorage', key, obj);
    },

    MyUtil.prototype.getLocalStorage = function(key) {
	if (this.isStorageAvailable('localStorage') !== true) return null;
        return this.getStorage('localStorage', key);
    },

    MyUtil.prototype.setStorage = function(type, key, obj) {
        let storage = window[type];
        storage.setItem(key, JSON.stringify(obj));
    },

    MyUtil.prototype.getStorage = function(type, key) {
        let storage = window[type];
        let str = storage.getItem(key);
        if (str) {
            let obj = JSON.parse(str);
            return obj;
        }
        else {
            return null;
        }
    },
    
    // export the constructor
    window.MyUtil = MyUtil;
}(jQuery)
